import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Product } from './product';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService { 
 
  constructor(private _http : Http) { }

  readCategories(): Observable<Product[]>{

  return this._http
    .get("http://localhost/masterapi/product/read.php")
    .pipe(map((res: Response) => res.json()));
  }
}