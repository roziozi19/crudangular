import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

import {map} from "rxjs/operators";

@Injectable({
    providedIn: 'root',
})
export class ProductService {

    constructor(private _http: Http) {
    }

    readProducts() {
        let url = 'http://localhost/ebdesktraining/masterapi/product/read.php';
        let getData = this._http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    readOneProducts(id) {
        let url = 'http://localhost/ebdesktraining/masterapi/product/read_one.php?id=' + id;
        let getData = this._http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    deleteOneProducts(product_id) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {id: product_id};
        let json = JSON.stringify(data);
        let url = 'http://localhost/ebdesktraining/masterapi/product/delete.php';
        let getData = this._http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    readCategories() {
        let url = 'http://localhost/ebdesktraining/masterapi/product/read.php';
        let getData = this._http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    updateProduct(id, name, price, description, category_id) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {id: id, name: name, price: price, description: description, category_id: category_id};
        let json = JSON.stringify(data);
        let url = 'http://localhost/ebdesktraining/masterapi/product/update.php';
        let getData = this._http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    createProduct(name, price, description, category_id) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {name: name, price: price, description: description, category_id: category_id};
        let json = JSON.stringify(data);
        let url = 'http://localhost/ebdesktraining/masterapi/product/create.php';
        let getData = this._http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))

    }
}
