import { Component, OnInit } from '@angular/core';


import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from "../product.service";


@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
    public subs;
    public params;
    public dataname;
    public dataprice;
    public datadescription;
    public datacategory;

    constructor(public router: Router, private route: ActivatedRoute, public service: ProductService) {
    }

  ngOnInit() {
      this.subs = this.route.params.subscribe(params => {
          this.params = params['some-data'];
      });

      this.service.readOneProducts(this.params).subscribe(data => {
          this.dataname = data.name;
          this.dataprice = data.price;
          this.datadescription = data.description;
      });

      this.service.readCategories().subscribe(data =>{
          this.datacategory = data['records'];
          for (var i = 0; i< this.datacategory.length; i++){
          }
      })

  }

  getData (name,prise,description,option){
      this.service.updateProduct(this.params,name,prise,description,option).subscribe(data =>{
          alert('edited success')
      })
  }

}
