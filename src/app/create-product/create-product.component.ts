import {Component, OnInit} from '@angular/core';
import {ProductService} from "../product.service";
import {ActivatedRoute, Router} from '@angular/router';


@Component({
    selector: 'app-create-product',
    templateUrl: './create-product.component.html',
    styleUrls: ['./create-product.component.css'],
})
export class CreateProductComponent implements OnInit {


    constructor(public router: Router, private route: ActivatedRoute, public service: ProductService) {
    }

    ngOnInit() {
    }

    create(name,prise,description,option){
        console.log(name,prise,description,option)
        this.service.createProduct(name,prise,description,option).subscribe(data =>{
            alert('create success')

        })
    }

}
